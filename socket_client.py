import socket

HOST = "127.0.0.1"
PORT = 65432        # (0...1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    while True:
        data = b"Hello!"
        if data:
            s.sendall(data)
            receive_data = s.recv(1024)
            print(f"received: {receive_data}")
    print("-" * 80)
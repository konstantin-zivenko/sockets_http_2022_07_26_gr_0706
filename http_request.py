import requests

# # GET
#
# requests.get("https://api.github.com")
#
# response = requests.get("https://api.github.com")
#
# if response.status_code == 200:
#     print('Success!')
# elif response.status_code == 404:
#     print('Not Found.')
#
#
# if response:            # (__bool__)
#     print("Success!!")
#
#
from requests.exceptions import HTTPError

for url in ["https://api.github.com", "https://api.github.com/invalid"]:
    try:
        response = requests.get(url)
        response.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except Exception as err:
        print(f"Other error occurred: {err}")
    else:
        print("Success!!")

content = response.content
print(content)


response.encoding = "utf-8"
text = response.text
print(text)

dict_response = response.json()
print(dict_response)
print(f"type of dict_response: {type(dict_response)}")

# Параметри запиту

response = requests.get(
    'https://api.github.com/search/repositories',
    params={'q': 'requests+language:python'},
)

json_response = response.json()
repository = json_response['items'][0]

print(f"Repository name: {repository['name']}")
print(f"Repository description: {repository['description']}")

response = requests.get(
    'https://api.github.com/search/repositories',
    params=[('q', 'requests+language:python')],
)

response = requests.get(
    'https://api.github.com/search/repositories',
    params={'q': 'requests+language:python'},
    headers={'Accept': 'application/vnd.github.v3.text-match+json'},
)

json_response = response.json()
repository = json_response['items'][0]
print(f'Text matches: {repository["text_matches"]}')

# POST, PUT, DELETE, PATCH, HEAD, OPTIONS

requests.post('https://httpbin.org/post', data={'key':'value'})
requests.post('https://httpbin.org/post', data=[('key', 'value')])
requests.put('https://httpbin.org/put', data={'key':'value'})
requests.delete('https://httpbin.org/delete')
requests.head('https://httpbin.org/get')
requests.patch('https://httpbin.org/patch', data={'key':'value'})
requests.options('https://httpbin.org/get')


